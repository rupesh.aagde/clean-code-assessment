import express from 'express'
import uuidv4 from 'uuid'
const app = express()

const products = []

// Add new product
app.post('/product', (req, res) => {
    try {
        const { name, description, quantity, price } = req.body;
        const newProduct = {
            id: uuidv4(),
            name,
            description,
            quantity,
            price
        };
        
        products.push(newProduct);
        res.status(200).json(newProduct);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// fetch product by ID
app.get('/products/:id', (req, res) => {
    try {
        const id = req.params.id;
        const product = products.find(product => product.id === id);
        if (!product) {
            throw new Error('Product not found');
        }
        res.json(product);
    } catch (err) {
        res.status(404).json({ message: err.message });
    }
});

// Update product by ID
app.put('/products/:id', (req, res) => {
    try {
        const id = parseInt(req.params.id);
        const { name, description, price, quantity } = req.body;
        const product = products.find(product => product.id === id);

        if (!product) {
            throw new Error('Product not found');
        }
        product.name = name;
        product.description = description;
        product.price = price;
        product.quantity = quantity;

        res.json(product);
    } catch (err) {
        res.status(404).json({ message: err.message });
    }
});

// Delete product by ID
app.delete('/products/:id', (req, res) => {
    try {
        const id = parseInt(req.params.id);
        const index = products.findIndex(product => product.id === id);

        if (index !== -1) {
            const deletedProduct = products.splice(index, 1);
            res.json(deletedProduct[0]);
        } else {
            throw new Error('Product not found');
        }
    } catch (err) {
        res.status(404).json({ message: err.message });
    }
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
